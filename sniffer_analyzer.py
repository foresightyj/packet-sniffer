import struct
import sys
from collections import namedtuple

#Advertising channel PDU Header's PDU Type field encoding
ADVERT_HEADER_PDU_TYPE = ['ADV_IND', 'ADV_DIRECT_IND', 'ADV_NONCONN_IND',
                          'SCAN_REQ', 'SCAN_RSP', 'CONNECT_REQ', 'ADV_SCAN_IND']
AD_TYPES = {
    0x01: 'Flags',
    0x02: 'Incomplete List of 16-bit Service Class UUIDs',
    0x03: 'Complete List of 16-bit Service Class UUIDs',
    0x04: 'Incomplete List of 32-bit Service Class UUIDs',
    0x05: 'Complete List of 32-bit Service Class UUIDs',
    0x06: 'Incomplete List of 128-bit Service Class UUIDs',
    0x07: 'Complete List of 128-bit Service Class UUIDs',
    0x08: 'Shortened Local Name',
    0x09: 'Complete Local Name',
    0x0A: 'Tx Power Level',
    0x0D: 'Class of Device',
    0x19: 'Appearance',
    0xFF: 'Manufacturer Specific Data',
    }

SniffData = namedtuple('SniffData', ['pack_info', 'pack_no', 'time', 'length', 'payload', 'FCS'])
ADVERT_ACCESS_ADDR = 0x8e89bed6L

def parse_psd_data(file_name='sniff.data.psd'):
    def get_psd_chunks():
        chunks = []
        each_size = 1+4+8+258
        with open(file_name,'rb') as f:
            data = f.read()
            no_chunks,zero = divmod(len(data), each_size)
            for i in range(no_chunks):
                yield data[i*each_size:(i+1)*each_size]
    sniff = []
    for chunk in get_psd_chunks():
        pack_info, pack_no, time, length = struct.unpack('<BIQB',chunk[0:14])
        pack_info_bit_0 = pack_info & 0x01
        if pack_info_bit_0 == 0:
            payload_length = length
        else:
            payload_length = length - 2
        payload = chunk[14:14+payload_length]

        FCS = chunk[14+payload_length:16+payload_length]

        #the rest of data are spare data which is discarded.
        if not sniff: #sniff empty
            first_time = time
            relative_time = 0
        else:
            relative_time = time-first_time

        sniff.append(SniffData(pack_info, pack_no, relative_time, length, payload, FCS))
    return sniff

def parse_SniffData(sniffdata):
    assert(isinstance(sniffdata, SniffData))

    def parse_advert(header, PDU_payload):
        def parse_adv_header():
            adv_pdu_type = header & 0x0f #see page 82 in the book
            tx_addr_type = 'Random' if (header & 0xc0)>>6 else 'NonRandom'
            rx_addr_type = 'Random' if header >> 7 else 'NonRandom'
            return adv_pdu_type, tx_addr_type, rx_addr_type

        def get_handler_by_adv_pdu_type(adv_pdu_type):
            def parse_adv_ind(AD_TYPES=AD_TYPES):
                def parse_ad_type_structure():
                    EXPECT_LEN, EXPECT_DATA = range(2)
                    state = EXPECT_LEN

                    ADStructs = []
                    ADStructLen = 0
                    ADStructData = 0

                    index = 0
                    while index < len(PDU_payload):
                        if state == EXPECT_LEN:
                            ADStructLen = advdatabytes[index]
                            index += 1
                            if ADStructLen:
                                state = EXPECT_DATA
                        else:
                            ADStructData = advdatabytes[index: index + ADStructLen]
                            ADStructs.append((ADStructLen, ADStructData))
                            state = EXPECT_LEN
                            index += ADStructLen


                    for a in ADStructs:
                        _, ADStructData = a

                        AD_Type = ADStructData[0]
                        Data = ADStructData[1:]
                        #print AD_TYPES[AD_Type],':', Data
                        if AD_Type == 0x09:
                            print AD_TYPES[AD_Type],':',
                            for d in Data:
                                print chr(d),
                        else:
                            print AD_TYPES[AD_Type], Data

                pass

            def parse_connect_req():
                InitA, AdvA, LLData = struct.unpack('<6s6s22s',PDU_payload)
                AA, CRCInit, WinSize, WinOffset, Interval, Latency, Timeout, ChM, Hop_and_SCA =\
                    struct.unpack('<4s3sBHHHH5ss', LLData)
                #connInterval = Interval *1.25ms
                #connSlaveLatency = Latency
                #connSupervisionTimeout = Timeout*10ms
                return Interval*1.25, Latency, Timeout*10

            if not hasattr(get_handler_by_adv_pdu_type, 'handler_map'):
                handler_map = {
                    0x00: parse_adv_ind,
                    #0x01: parse_adv_direct_ind,
                    #0x02: parse_adv_nonconn_ind,
                    #0x03: parse_scan_req,
                    #0x04: parse_scan_rsp,
                    0x05: parse_connect_req,
                    #0x06: parse_adv_scan_ind,
                }
            return handler_map.get(adv_pdu_type, lambda *args: sys.stderr.write('Error: handler not implemented!\r\n'))

        adv_pdu_type,_,_ = parse_adv_header()
        get_handler_by_adv_pdu_type(adv_pdu_type+1)()

    def parse_data(): #BLE data channel
        raise Exception('No implemented yet')

    access_addr, header, length = struct.unpack('<IBB', sniffdata.payload[0:6])
    if access_addr == ADVERT_ACCESS_ADDR: #advert and data packets are differentiated by their access addr
        PDU_payload = sniffdata.payload[6:6+length]
        return parse_advert(header, PDU_payload)
    else:
        return parse_data()

    raise Exception('Weird SniffData.')


sniff = parse_psd_data()

print parse_SniffData(sniff[0])



