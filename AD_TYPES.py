advdatabytes = 'D6 BE 89 8E 00 18 47 15 00 61 02 00 02 01 06 07 03 03 18 02 18 04 18 03 19 00 02 02 0A FE 13 B2 B0 30 A6'.split()

advdatabytes = '2\x15\x00a\x02\x00\x02\x01\x06\x07\x03\x03\x18\x02\x18\x04\x18\x03\x19\x00\x02\x02\n\xfe\t\tAS-D1532'


print advdatabytes

#AD TYPES can be found in https://www.bluetooth.org/Technical/AssignedNumbers/generic_access_profile.htm

AD_TYPES = {
    0x01: 'Flags',
    0x02: 'Incomplete List of 16-bit Service Class UUIDs',
    0x03: 'Complete List of 16-bit Service Class UUIDs',
    0x04: 'Incomplete List of 32-bit Service Class UUIDs',
    0x05: 'Complete List of 32-bit Service Class UUIDs',
    0x06: 'Incomplete List of 128-bit Service Class UUIDs',
    0x07: 'Complete List of 128-bit Service Class UUIDs',
    0x08: 'Shortened Local Name',
    0x09: 'Complete Local Name',
    0x0A: 'Tx Power Level',
    0x0D: 'Class of Device',
    0x19: 'Appearance',
    0xFF: 'Manufacturer Specific Data',
    }


EXPECT_LEN, EXPECT_DATA = range(2)
state = EXPECT_LEN

ADStructs = []
ADStructLen = 0
ADStructData = 0

index = 0
while index < len(advdatabytes):
    if state == EXPECT_LEN:
        ADStructLen = advdatabytes[index]
        index += 1
        if ADStructLen:
            state = EXPECT_DATA
    else:
        ADStructData = advdatabytes[index: index + ADStructLen]
        ADStructs.append((ADStructLen, ADStructData))
        state = EXPECT_LEN
        index += ADStructLen


for a in ADStructs:
    _, ADStructData = a

    AD_Type = ADStructData[0]
    Data = ADStructData[1:]
    #print AD_TYPES[AD_Type],':', Data
    if AD_Type == 0x09:
        print AD_TYPES[AD_Type],':',
        for d in Data:
            print chr(d),
    else:
        print AD_TYPES[AD_Type], Data





