#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      AdminNUS
#
# Created:     20/03/2013
# Copyright:   (c) AdminNUS 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------

from construct import *
data = ''.join(map(lambda i: chr(int(i,16)), '11 00'.split()))

DATA_HEADER = BitStruct("data_header",
        Padding(3),
        Flag("MD"),
        Flag("SN"),
        Flag("NESN"),
        Enum(BitField("LLID", 2),
            LLID_RESERVED = 0b00,
            L2CAP_C = 0b01,
            L2CAP_S = 0b10,
            LL_CONTROL_PDU = 0b11,
        ),
        Padding(3),
        BitField("PDU_length", 5),
    )

print DATA_HEADER.parse(data)