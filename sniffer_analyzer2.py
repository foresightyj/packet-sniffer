#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      yuanjian
#
# Created:     18/03/2013
# Copyright:   (c) yuanjian 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import sys
from construct import *
import pdb
import pprint
import wx
import os

class myprint():
    def write(self, stuff):
        pprint.pprint(stuff)

mypri = myprint()

def coroutine(func):
    def start(*args,**kwargs):
        g = func(*args,**kwargs)
        g.next()
        return g
    return start

def parse_psd_data(file_name='broadcaster_observer_latency.psd'):
    def get_psd_chunks():
        chunks = []
        each_size = 1+4+8+258
        with open(file_name,'rb') as f:
            data = f.read()
            no_chunks,zero = divmod(len(data), each_size)
            for i in range(no_chunks):
                yield data[i*each_size:(i+1)*each_size]

    psd = Struct("PSD",
        BitStruct("packet_info",
            BitField("not_used", 5),
            Bit("incomplete_packet"),
            Bit("correlation_used"),
            Bit("length_included_FCS")
        ),
        ULInt32("packet_no"),
        ULInt64("time"),
        ULInt8("length"),
        Field("payload", lambda ctx: ctx.length if ctx.packet_info.length_included_FCS == 0 else ctx.length - 2),
        #the rest are just junk bytes
    )

    sniff = []
    for chunk in get_psd_chunks():
        parsed = psd.parse(chunk)
        yield parsed

def get_AD_structures_parser():
    AD_STRUCTURE = Struct("AD_STRUCTURE",
        ULInt8("length"),
        ULInt8("AD_Type"),
        Field("AD_Data", lambda ctx: ctx.length-1),
    )
##    AD_STRUCTURES = RepeatUntil(
##        lambda obj, ctx: obj.AD_Data_start_pos + obj.length > length,
##        AD_STRUCTURE,
##    )

    AD_STRUCTURES = RepeatUntil(
        lambda obj, ctx: obj == None,
        Optional(AD_STRUCTURE),
    )

    return lambda AD_structures, length:AD_STRUCTURES.parse(AD_structures)

@coroutine
def link_layer_parser():
    """ a coroutine that parses link layer payload
        input: PSD payload, which is actually a link layer packet without preamble
        link layer payload format (where number indicates no of bits): see page 30 in book and page 2200 in spec
        [8 preamble][32 access addr][8 header][0 to 296 data][24 CRC]
    """
    ADVERT_ACCESS_ADDR = 0x8e89bed6L

    # Be careful of the endianess.
    ADVERT_HEADER = BitStruct("advert_header",
            Flag("RxAdd"),
            Flag("TxAdd"),
            Padding(2),
            Enum(BitField("PDU_Type", 4),
                ADV_IND = 0b0000,
                ADV_DIRECT_IND = 0b0001,
                ADV_NONCONN_IND = 0b0010,
                SCAN_REQ = 0b0011,
                SCAN_RSP = 0b0100,
                CONNECT_REQ = 0b0101,
                ADV_SCAN_IND = 0b0110,
                ),
            Padding(2),
            BitField("PDU_length", 6),
        )

    AD_STRUCTURE = Struct("AD_STRUCTURE",
        ULInt8("length"),
        #ULInt8("AD_Type"),
        Enum(Byte("AD_Type"),
            FLAGS = 0x01,
            INCOMPLETE_LIST_OF_16_BIT_SERVICE_CLASS_UUIDS = 0x02,
            COMPLETE_LIST_OF_16_BIT_SERVICE_CLASS_UUIDS = 0x03,
            INCOMPLETE_LIST_OF_32_BIT_SERVICE_CLASS_UUIDS = 0x04,
            COMPLETE_LIST_OF_32_BIT_SERVICE_CLASS_UUIDS = 0x05,
            INCOMPLETE_LIST_OF_128_BIT_SERVICE_CLASS_UUIDS = 0x06,
            COMPLETE_LIST_OF_128_BIT_SERVICE_CLASS_UUIDS = 0x07,
            SHORTENED_LOCAL_NAME = 0x08,
            COMPLETE_LOCAL_NAME = 0x09,
            TX_POWER_LEVEL = 0x0A,
            CLASS_OF_DEVICE = 0x0D,
            APPEARANCE = 0x19,
            MANUFACTURER_SPECIFIC_DATA = 0xFF,
            _default_ = 'Unknown AD_Type',
        ),
        Field("AD_Data", lambda ctx: ctx.length-1),
        Anchor("__tail_pos"), # starting with __ will cause it not to show in Containers.
    )

    ADVDATA = Struct("AdvData", #For both AdvData and ScanRspData
        Anchor("__start_pos"),
        RepeatUntil(
            lambda obj, ctx: obj.__tail_pos - ctx.__start_pos == ctx._._.PDU_length - 6,
            AD_STRUCTURE,
        ),
    )

    ADV_IND = Struct("ADV_IND",
        Field("AdvA", 6),
        #Field("AdvData", lambda ctx: ctx._.PDU_length-6)
        Optional(ADVDATA),
    )
    ADV_DIRECT_IND = Struct("ADV_DIRECT_IND",
        Field("AdvA", 6),
        Field("InitA", 6),
    )
    ADV_NONCONN_IND = Rename("ADV_NONCONN_IND",ADV_IND)
    ADV_SCAN_IND = Rename("ADV_SCAN_IND",ADV_IND)
    SCAN_REQ = Struct("SCAN_REQ",
        Field("ScanA", 6),
        Field("AdvA", 6),
    )
    SCANRSPDATA = Rename('ScanRspData', ADVDATA)
    SCAN_RSP = Struct("SCAN_RSP",
        Field("AdvA", 6),
        #Field("ScanRspData", lambda ctx: ctx._.PDU_length-6),
        Optional(SCANRSPDATA), #could be empty so we need Optional here.
    )
    CONNECT_REQ = Struct("CONNECT_REQ",
        Field("InitA", 6),
        Field("AdvA", 6),
        Struct("LLData",
            Field("AA", 4),
            Field("CRCInit", 3),
            ULInt8("WinSize"),
            ULInt16("WinOffset"),
            ULInt16("Interval"),
            ULInt16("Latency"),
            ULInt16("Timeout"),
            Field("ChM", 5),
            Embed(BitStruct("foo",
                    BitField("SCA",3),
                    BitField("Hop",5),
                )
            )
        )
    )

    ADVERT_PDU = Struct("ADVERT_PDU",
        Embed(ADVERT_HEADER),
        #Field("payload", lambda ctx: ctx.PDU_length),
        Switch("payload", lambda ctx: ctx.PDU_Type,
            {
                'ADV_IND'           : ADV_IND,
                'ADV_DIRECT_IND'    : ADV_DIRECT_IND,
                'ADV_NONCONN_IND'   : ADV_NONCONN_IND,
                'ADV_SCAN_IND'      : ADV_SCAN_IND,
                'SCAN_REQ'          : SCAN_REQ,
                'SCAN_RSP'          : SCAN_RSP,
                'CONNECT_REQ'       : CONNECT_REQ,
            }
        )
    )

    DATA_HEADER = BitStruct("data_header",
            Padding(3),
            Flag("MD"),
            Flag("SN"),
            Flag("NESN"),
            Enum(BitField("LLID", 2),
                LLID_RESERVED = 0b00,
                L2CAP_C = 0b01,
                L2CAP_S = 0b10,
                LL_CONTROL_PDU = 0b11,
            ),
            Padding(3),
            BitField("PDU_length", 5),
    )

    LL_CONTROL_PDU = Struct("LL_CONTROL_PDU",
        Enum(ULInt8("Opcode"),
            LL_CONNECTION_UPDATE_REQ      = 0x00,
            LL_CHANNEL_MAP_REQ            = 0x01,
            LL_TERMINATE_IND              = 0x02,
            LL_ENC_REQ                    = 0x03,
            LL_ENC_RSP                    = 0x04,
            LL_START_ENC_REQ              = 0x05,
            LL_START_ENC_RSP              = 0x06,
            LL_UNKNOWN_RSP                = 0x07,
            LL_FEATURE_REQ                = 0x08,
            LL_FEATURE_RSP                = 0x09,
            LL_PAUSE_ENC_REQ              = 0x0a,
            LL_PAUSE_ENC_RSP              = 0x0b,
            LL_VERSION_IND                = 0x0c,
            LL_REJECT_IND                 = 0x0d,
            _default_                     = "Reserved for Future Use",
        ),
        Field("CtrData", lambda ctx: ctx._.PDU_length-1)
    )

    DATA_PDU = Struct("DATA_PDU",
        Embed(DATA_HEADER),
        #Probe(),
        IfThenElse("LL_DATA_PDU", lambda ctx: ctx.LLID == 'LL_CONTROL_PDU',
            LL_CONTROL_PDU,
            Field("foo", lambda ctx: 0),
        )
    )

    ll_packet = Struct("link_layer_packet",
        ULInt32("access_addr"),
        Value("packet_type", lambda ctx: 'ADVERT' if ctx.access_addr == ADVERT_ACCESS_ADDR else 'DATA'),
        IfThenElse("PDU", lambda ctx: ctx.packet_type is 'ADVERT',
            ADVERT_PDU,
            DATA_PDU,
        ),
        Field("CRC", 3),
        ULInt8("RSSI"),
        Embed(BitStruct("foo",
            Bit("CRC_OK"),
            BitField("Channel",7),
            )
        ),
    )

    try:
        print 'Ready to parse Link Layer Packets'
        parsed = None
        while True:
            psd_data = (yield)
            try:
                parsed = ll_packet.parse(psd_data.payload)
            except:
                print >>sys.stderr, 'Parsing packet no %d failed' % psd_data.packet_no
                continue

            if parsed.packet_type == "ADVERT" and parsed.PDU.PDU_Type == 'ADV_IND':
                print >>mypri, parsed
            elif parsed.packet_type == "DATA" and parsed.PDU.LLID == 'LL_CONTROL_PDU':
                pass
            print parsed

    except GeneratorExit:
        print 'Done parsing Link Layer Packets'

def timestamp2timeus (timeuint64):
    timelo = timeuint64 & 0xffff
    timehi = (timeuint64>>16)
    return ((timehi*5000) + timelo)/32

def main():
    #set up the pipeline
    ll_parser = link_layer_parser()

    app = wx.PySimpleApp()
    dlg = wx.FileDialog(None, "Choose a file", os.getcwd(), "", '*.psd', wx.OPEN)
    if dlg.ShowModal() == wx.ID_OK:
        filename = dlg.GetPath()
    dlg.Destroy()

    for psd_data in parse_psd_data(filename):
        ll_parser.send(psd_data)

    ll_parser.close()

if __name__ == '__main__':
    main()
